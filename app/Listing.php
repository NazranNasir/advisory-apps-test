<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Listing extends Model
{
    
    protected $fillable = [
        'list_name', 'distance', 'user_id'
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
