<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\ListingResource;
use App\Listing;
use Auth;

class ListingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function allListings()
    {
        $listings = Listing::all();
        return ListingResource::collection($listings);
    }

    public function myListings($id)
    {
        $listings = Listing::where('user_id', $id)->get();
        return ListingResource::collection($listings);
    }

    public function index(Request $request)
    {
        $listings = Listing::all();
        $currentListings = Listing::where('user_id', Auth::user()->id)->get();

        return view('listings.index', compact('listings', 'currentListings'));
        // return redirect()->back()->with('status', 'Listing retrieved successfully!', compact('data')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new Listing;

        $data->list_name = $request->list_name;
        $data->distance = $request->distance;
        $data->user_id = $request->user_id;
        $data->save();

        return redirect()->back()->with('status', 'New Listing added successfully!'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $listing = Listing::find($id);
        
        return view('listings.show', compact('listing'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Listing::find($id);
        $data->list_name = $request->list_name;
        $data->distance = $request->distance;
        $data->save();

        return redirect()->back()->with('status', 'Listing updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Listing::find($id);

        $data->delete();
        
        $listings = Listing::all();
        $currentListings = Listing::where('user_id', Auth::user()->id)->get();

        return redirect('listings')->with('status', 'Listing deleted successfully!', compact('listings', 'currentListings'));
    }
}
