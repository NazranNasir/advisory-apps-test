@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-sm-8">
            <div class="card">
                <div class="card-header">
                    List ID: {{ $listing->id }}
                </div>
                <div class="card-body">
                    {!! Form::open(array('route' => ['listings.update', $listing->id], 'method' => 'put')) !!}
                        <p class="card-text">
                            Name : {!! Form::text('list_name', $listing->list_name, ['class' => 'form-control', 'id' => 'list_name', 'placeholder' => 'Enter List Name', 'required']) !!}
                            Distance (km): {!! Form::text('distance', $listing->distance, ['class' => 'form-control', 'id' => 'distance', 'placeholder' => 'Enter Distance (km)', 'required']) !!}<br>
                            User: {{ $listing->user->name }} ( ID: {{ $listing->user_id }} )
                        </p>
                        <div class="btn-group">
                            <button type="submit" class="btn btn-primary">Update</button>
                    {!! Form::close() !!}
                    {!! Form::open(array('route' => ['listings.destroy', $listing->id], 'method' => 'delete')) !!}
                            <button type="submit" class="btn btn-danger ml-2">Delete</button>
                        </div>
                    {!! Form::close() !!}
                    <a class="btn" href="{{ route('home') }}">Go Home</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
