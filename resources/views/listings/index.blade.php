@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-sm-6">
            <h3>All List ({{ $listings->count() }})</h3>
            <div class="list-group">
                @foreach($listings as $listing)
                    {!! Form::open(array('route' => ['listings.show', $listing->id], 'method' => 'get')) !!}
                        <button type="submit" class="list-group-item list-group-item-action list-group-custom">
                            {{ $listing->list_name }}
                        </button>
                    {!! Form::close() !!}
                @endforeach
            </div>
        </div>
        <div class="col-sm-6">
            <h3>My List ({{ $currentListings->count() }})</h3>
            <div class="list-group">
                @foreach($currentListings as $listing)
                    {!! Form::open(array('route' => ['listings.show', $listing->id], 'method' => 'get')) !!}
                        <button type="submit" class="list-group-item list-group-item-action list-group-custom">
                            {{ $listing->list_name }}
                        </button>
                    {!! Form::close() !!}
                @endforeach
            </div>
        </div>
    </div>
    <a class="btn" href="{{ route('home') }}">Go Home</a>
</div>
@endsection
