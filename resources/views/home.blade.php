@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8">
            <h4>API</h4>
            <div class="btn-group">
                {!! Form::open(array('route' => 'allListings', 'method' => 'get')) !!}
                    <button type="submit" class="btn btn-primary">Get All Listing</button>
                {!! Form::close() !!}
                {!! Form::open(array('route' => ['myListings', Auth::user()->id], 'method' => 'get')) !!}
                    <button type="submit" class="btn btn-success ml-2">Get My Listing</button>
                {!! Form::close() !!}
                {!! Form::open(array('route' => ['detail', Auth::user()->id], 'method' => 'get')) !!}
                    <button type="submit" class="btn btn-warning ml-2">Get Current User</button>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-8">
            <h4>List</h4>
            <div class="btn-group">
                {!! Form::open(array('route' => 'listings.index', 'method' => 'get')) !!}
                    <button type="submit" class="btn btn-primary">Go to List Page</button>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h4>Add Listing</h4>
                </div>
                <div class="card-body">
                    {!! Form::open(array('route' => 'listings.store', 'method' => 'post')) !!}
                        {!! Form::hidden('user_id', Auth::user()->id) !!}
                        <div class="form-group row">
                            <label for="list_name" class="col-sm-2 col-form-label">List Name</label>
                            <div class="col-sm-10">
                                {!! Form::text('list_name', null, ['class' => 'form-control', 'id' => 'list_name', 'placeholder' => 'Enter List Name', 'required']) !!}
                            </div>
                            <label for="distance" class="col-sm-2 col-form-label">Distance (km)</label>
                            <div class="col-sm-10">
                                {!! Form::text('distance', null, ['class' => 'form-control', 'id' => 'distance', 'placeholder' => 'Enter Distance (km)', 'required']) !!}
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary">Add</button>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
