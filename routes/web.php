<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('user_detail/{id}', 'UserController@detail')->name('detail');
Route::get('my_listings/{id}', 'ListingController@myListings')->name('myListings');
Route::get('all_listings', 'ListingController@allListings')->name('allListings');
Route::resource('listings', 'ListingController');